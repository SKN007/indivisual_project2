rust-version:
	@echo "Rust command-line utility versions:"
	rustc --version 			#rust compiler
	cargo --version 			#rust package manager
	rustfmt --version			#rust code formatter
	rustup --version			#rust toolchain manager
	clippy-driver --version		#rust linter

format:
	cargo fmt --quiet

install:
	# Install if needed
	#@echo "Updating rust toolchain"
	#rustup update stable
	#rustup default stable 

# Name of the Docker image
IMAGE_NAME=indivisual_project2

# Build the Docker image
docker-build:
	docker build -t $(IMAGE_NAME) .

# Run the Docker container
docker-run:
	docker run -p 8080:8080 $(IMAGE_NAME)

lint:
	cargo clippy --quiet

test:
	cargo test --quiet

build: 
	cargo build --release 

all: format lint test build
