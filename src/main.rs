use actix_files as fs;
use actix_web::{web, App, HttpResponse, HttpServer, Responder};
use serde::{Deserialize, Serialize};

#[derive(Deserialize)]
struct SumRequest {
    a: f64,
    b: f64,
}

#[derive(Serialize)]
struct SumResponse {
    sum: f64,
}

async fn calculate_sum(sum_request: web::Json<SumRequest>) -> impl Responder {
    let sum = sum_request.a + sum_request.b;
    HttpResponse::Ok().json(SumResponse { sum })
}

#[actix_web::main]
async fn main() -> std::io::Result<()> {
    HttpServer::new(|| {
        App::new()
            .service(web::resource("/sum").route(web::post().to(calculate_sum)))
            .service(fs::Files::new("/", "./static/").index_file("index.html")) // 添加静态文件服务
    })
    .bind("127.0.0.1:8080")?
    .run()
    .await
}
