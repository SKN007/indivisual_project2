
[![pipeline status](https://gitlab.com/SKN007/indivisual_project2/badges/master/pipeline.svg)](https://gitlab.com/SKN007/indivisual_project2/-/commits/master)

# Individual Project 2

## Requirements

    Simple REST API/web service in Rust

    Dockerfile to containerize service

    CI/CD pipeline files

## Steps

1. Create a new Rust Actix Web Application.

   ```
   cargo new indivisual_project2
   ```
2. Implement all the functions in src/main.rs. The function I implemented is calculate two number's sum. Also add all the dependencies in Cargo.toml.
3. Create a folder 'static' and create a index.html for website UI design.
4. Write Makefile and Dockerfile.
5. Build and run the project.

   ```
   cargo build
   cargo run
   ```
6. Open dockerdesktop and build the image.

   ```
   docker build -t indivisual_project2 .
   ```
7. Finally, we can see the work on 127.0.0.1:8080.

## Screenshot Display

![img](docker.png)


![img](result.png)

## Demo Video

[![Video Name]( "Demo")](demo.mp4)
