# Use the official Rust image as a builder
FROM rust:1-slim-bookworm AS builder

# Create a new empty shell project
RUN USER=root cargo new --bin indivisual_project2
WORKDIR /indivisual_project2

# Copy the Cargo.toml and Cargo.lock files and build the dependencies
COPY Cargo.toml Cargo.lock ./
RUN cargo build --release
RUN rm src/*.rs

# Copy the source code
COPY ./src ./src
COPY ./static ./static

# Build the application for release
RUN rm -f ./target/release/deps/indivisual_project2*
RUN cargo build --release

# Final base image
FROM bitnami/minideb:bookworm

# Copy the build artifact from the builder stage and set permissions
COPY --from=builder /indivisual_project2/target/release/indivisual_project2 /usr/local/bin

ENV ROCKET_ENV=production

# Set the binary as the entrypoint of the container
ENTRYPOINT ["/usr/local/bin/indivisual_project2"]

# Expose the port the server is running on
EXPOSE 8080
